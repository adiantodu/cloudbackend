function toggleMenu(){
    var x = document.querySelector('.nav_btn__content')
    if(x.style.display != "block") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function accordMenu(){
    var r = document.querySelectorAll('.nav_btn__content__link > .head')
    for(let i=0; i<r.length; i++){
        if(r[i] != event.currentTarget){
            r[i].classList.remove('active')
        }
    }
    event.currentTarget.classList.toggle('active')
    var c = event.currentTarget.nextElementSibling
    if(!c.classList.contains('active')){
        var nodeList = document.querySelectorAll(".nav_btn__content__link > .body")
        for (let i = 0; i < nodeList.length; i++) {
            nodeList[i].classList.remove('active')
            nodeList[i].style.maxHeight = null;
        }
        c.classList.add('active')
        c.style.maxHeight = (c.scrollHeight+24) + "px";
    }else{
        c.classList.remove('active')
        c.style.maxHeight = null;
    }
}